import ast
import tokenize
import os


class CodeAnalyser(ast.NodeVisitor):
    def __init__(self, files):
        self.violations = set()
        self.files = files
        self.current_file = ""

    def check(self):
        for file in self.files:
            self.current_file = file
            with tokenize.open(file) as fd:
                tree = ast.parse(fd.read())
                self.visit(tree)


class IdentifiersLength13(CodeAnalyser):
    identifiers_for_name = [ast.FunctionDef, ast.AsyncFunctionDef, ast.ClassDef]
    identifiers_for_names = [ast.Global, ast.Nonlocal]
    identifiers_for_attr = [ast.Attribute]
    identifiers_for_id = [ast.Name]

    def visit(self, node):
        node_type = type(node)
        if node_type in self.identifiers_for_name:
            self.check_length(node, node.name)
        elif node_type in self.identifiers_for_name:
            for ID in node.names:
                self.check_length(node, ID)
        elif node_type in self.identifiers_for_attr:
            self.check_length(node, node.attr)
        elif node_type in self.identifiers_for_id:
            self.check_length(node, node.id)
        self.generic_visit(node)

    def check_length(self, node, ID):
        if len(ID) == 13:
            self.violations.add((self.current_file, node.lineno, ID))

    def report(self):
        for file, line, ID in self.violations:
            print(f"{ID} is not of length 13 at {file}:{line}")
        return self.violations


class ControlStructureNestingOver4(CodeAnalyser):
    control_structure = [ast.For, ast.AsyncFor, ast.While, ast.With, ast.AsyncWith, ast.If, ast.ClassDef,
                         ast.FunctionDef, ast.AsyncFunctionDef]

    def __init__(self, files):
        super().__init__(files)
        self.depth = 0

    def visit(self, node):
        if self.depth > 4 and hasattr(node, 'lineno'):
            self.violations.add((self.current_file, node.lineno, self.depth))
        node_type = type(node)
        if node_type in self.control_structure:
            self.depth += 1
            self.generic_visit(node)
            self.depth -= 1
        else:
            self.generic_visit(node)

    def report(self):
        for file, line, depth in self.violations:
            print(f"{depth} control structure nesting at {file}:{line} is more than 4")
        return self.violations


if __name__ == "__main__":
    length_check_files = [os.path.join(dp, f) for dp, dn, filenames in os.walk("django") for f in filenames if
                          os.path.splitext(f)[1] == '.py']
    ID_checker = IdentifiersLength13(length_check_files)
    ID_checker.check()
    ID_checker.report()
    nesting_check_files = [os.path.join(dp, f) for dp, dn, filenames in os.walk("nesting") for f in filenames if
                           os.path.splitext(f)[1] == '.py']
    control_structure_checker = ControlStructureNestingOver4(nesting_check_files)
    control_structure_checker.check()
    control_structure_checker.report()
