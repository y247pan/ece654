class AA:
    def __init__(self):
        self.aa = 1

    def instance_function(self):
        global ff
        return self.aa + ff

    @staticmethod
    def static_function():
        bb = 10
        return bb


def CC(dd):
    ee = 1
    return dd + ee


ff = 10
if ff > 10:
    print(ff)
else:
    print(ff - 10)

for i in range(ff):
    print(ff)

gg = 0
while gg < ff:
    ff += 1

hh = [i for i in range(gg)]

abcdefghijklm = "bad variable name"
