import unittest
from A1 import IdentifiersLength13, ControlStructureNestingOver4


class TestString(unittest.TestCase):

    def setUp(self):
        self.identifiers_length_tp_files = ['test_identifiers_length_tp.py']
        self.control_structure_nesting_tp_files = ['test_control_structure_nesting_tp.py']
        self.ID_checker = IdentifiersLength13(self.identifiers_length_tp_files)
        self.control_structure_checker = ControlStructureNestingOver4(self.control_structure_nesting_tp_files)

    def check_result(self, actual_violations, expected_violations):
        self.assertEqual(len(actual_violations), len(expected_violations), "checker does not detect all problems")
        for actual_violation, expected_violation in zip(actual_violations, expected_violations):
            self.assertEqual(actual_violation[0], expected_violation[0])
            self.assertEqual(actual_violation[1], expected_violation[1])
            self.assertEqual(actual_violation[2], expected_violation[2])

    def test_identifiers_length_tp(self):
        self.ID_checker.check()
        actual_violations = sorted(list(self.ID_checker.report()))
        expected_violations = sorted(list({("test_identifiers_length_tp.py", 35, 'abcdefghijklm')}))
        self.check_result(actual_violations, expected_violations)

    def test_control_structure_nesting_tp(self):
        self.control_structure_checker.check()
        actual_violations = sorted(list(self.control_structure_checker.report()))
        expected_violations = sorted(list({("test_control_structure_nesting_tp.py", 5, 5),
                                           ("test_control_structure_nesting_tp.py", 6, 5)}))
        self.check_result(actual_violations, expected_violations)

    def tearDown(self):
        pass


if __name__ == "__main__":
    unittest.main()
